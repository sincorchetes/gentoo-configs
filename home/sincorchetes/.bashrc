# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.
QT_QPA_PLATFORMTHEME=qt5ct
export PATH=$PATH:~/.bin/
alias emi="doas emerge -av"
alias ems="emerge --search"
alias emr="doas emerge --unmerge"
alias emuf="doas vim /etc/portage/package.use/use_flags"
alias emak="doas vim /etc/portage/package.accept_keywords/allowed"
alias emu="doas emerge --sync && doas emerge -avuDN world"
alias emgk="doas genkernel --luks --lvm all && doas grub-mkconfig -o /boot/grub/grub.cfg"
alias emf="equery files"
alias ls="exa"
if [ -n "$DESKTOP_SESSION" ];then
   eval $(gnome-keyring-daemon --start)
   export SSH_AUTH_SOCK
fi
export PATH="/usr/lib/ccache/bin${PATH:+:}${PATH}"
export CCACHE_DIR="/var/cache/ccache"
